import 'package:flutter/material.dart';
import 'package:test/ui_kit/ui_kit.dart';

import 'form/order_page.dart';
import 'form/order_scope.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ApplicationTheme().theme,
        themeMode: ThemeMode.light,
        home: const OrderScope(
          child: OrderPage(),
        ));
  }
}
