import 'package:flutter/material.dart';

final class PaddingsExtension extends ThemeExtension<PaddingsExtension> {
  const PaddingsExtension({required this.small, required this.middle, required this.large});

  final double small;
  final double middle;
  final double large;

  @override
  ThemeExtension<PaddingsExtension> copyWith({
    double? small,
    double? middle,
    double? large,
  }) =>
      PaddingsExtension(small: small ?? this.small, middle: middle ?? this.middle, large: large ?? this.large);

  @override
  ThemeExtension<PaddingsExtension> lerp(covariant ThemeExtension<PaddingsExtension>? other, double t) {
    return PaddingsExtension(small: small, middle: middle, large: large);
  }
}
