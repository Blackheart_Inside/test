import 'package:flutter/material.dart';

import 'paddings_extensions.dart';

extension BuildContextThemeX on BuildContext {
  ThemeData get theme => Theme.of(this);

  PaddingsExtension get paddings => theme.extension<PaddingsExtension>()!;
  ColorScheme get colorsScheme => theme.colorScheme;
  TextTheme get textTheme => theme.textTheme;
}
