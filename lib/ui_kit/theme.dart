import 'package:flutter/material.dart';

import 'paddings_extensions.dart';

final class ApplicationTheme {
  static final ApplicationTheme _internalSingleton = ApplicationTheme._internal();
  factory ApplicationTheme() => _internalSingleton;
  ApplicationTheme._internal();

  late final ThemeData theme = ThemeData(
      useMaterial3: true,
      extensions: const [PaddingsExtension(small: 12.0, middle: 16.0, large: 20.0)],
      colorScheme: const ColorScheme.light(
        primary: Color(0xFFEA560D),
        errorContainer: Color(0xFF172027),
        onErrorContainer: Colors.red,
        onPrimary: Colors.white,
        background: Color(0xFFFAFAFA),
        onBackground: Colors.white,
      ),
      textButtonTheme: const TextButtonThemeData(
          style: ButtonStyle(padding: MaterialStatePropertyAll(EdgeInsets.zero), visualDensity: VisualDensity.compact)),
      filledButtonTheme: const FilledButtonThemeData(
          style: ButtonStyle(
        textStyle: MaterialStatePropertyAll(TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, height: 1.1)),
      )),
      appBarTheme: const AppBarTheme(
        iconTheme: IconThemeData(color: Color(0xFF172027)),
      ),
      textTheme: const TextTheme(
          titleMedium:
              TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400, color: Color(0xFF172027), letterSpacing: 1.16),
          titleLarge: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: Color(0xFF172027)),
          bodyMedium: TextStyle(fontSize: 14.0, color: Color(0xFF172027), fontWeight: FontWeight.w600, height: 1.07),
          bodyLarge: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, color: Color(0xFF172027))),
      inputDecorationTheme: InputDecorationTheme(
          contentPadding: const EdgeInsets.all(12.0),
          filled: false,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: const BorderSide(width: 0.50, color: Color(0xFFA9B5C1))),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: const BorderSide(width: 0.50, color: Color(0xFFA9B5C1))),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8.0),
              borderSide: const BorderSide(width: 0.50, color: Color(0xFFA9B5C1))),
          prefixIconColor: const Color(0xFF677482)));
}
