import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:test/ui_kit/ui_kit.dart';

import 'order_form.dart';
import 'order_scope.dart';
import 'widget/text_field.dart';
import 'widget/text_field_wrapper.dart';

final class OrderPage extends StatelessWidget {
  const OrderPage({super.key});

  @override
  Widget build(BuildContext context) {
    final textTheme = context.textTheme;
    final paddings = context.paddings;
    final locale = Localizations.localeOf(context);
    final startDateFormatter = DateFormat.yMMMMd(locale.toLanguageTag());
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ordering'),
        centerTitle: true,
        leading: const BackButtonIcon(),
        notificationPredicate: (notification) => false, //ignore scroll, material 3 behaviour
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Center(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: paddings.small, horizontal: paddings.large),
              child: Text(
                'Step 1',
                style: textTheme.titleMedium,
              ),
            ),
          ),
          Expanded(
            child: CustomScrollView(
              slivers: [
                SliverPadding(
                  padding: EdgeInsets.only(bottom: paddings.middle),
                  sliver: SliverToBoxAdapter(
                    child: ColoredBox(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: paddings.small, horizontal: paddings.large),
                        child: TextFieldWrapper(
                          title: const Text('Start date'),
                          textField: Builder(builder: (context) {
                            final scopeController = OrderScope.maybeOf(context);
                            final startDate = OrderScope.maybeGetStartDate(context);

                            return CustomTextField(
                              initialValue: startDateFormatter.format(startDate ?? DateTime.now()),
                              maxLine: 1,
                              readOnly: true,
                              onTap: () async {
                                final result = await showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime.now(),
                                    lastDate: DateTime.now().add(const Duration(days: 365 * 4)));
                                if (result != null && result != startDate) {
                                  scopeController?.changeStartDate(result);
                                }
                              },
                              icon: Icons.calendar_month,
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverList(
                    delegate: SliverChildListDelegate.fixed([
                  Builder(
                    builder: (context) => OrderFormWidget(
                        title: 'Sender details',
                        model: OrderScope.getByType(context, OrderDataType.sender),
                        onChange: (value) => OrderScope.maybeOf(context)?.changeSender(value)),
                  ),
                  Builder(
                    builder: (context) => OrderFormWidget(
                        title: 'Recipient address',
                        model: OrderScope.getByType(context, OrderDataType.recipient),
                        onChange: (value) => OrderScope.maybeOf(context)?.changeRecipient(value)),
                  )
                ]))
              ],
            ),
          ),
          ColoredBox(
            color: context.colorsScheme.onBackground,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: paddings.large).copyWith(top: paddings.middle),
              child: Builder(
                builder: (context) {
                  final sender = OrderScope.getByType(context, OrderDataType.sender);
                  final recipient = OrderScope.getByType(context, OrderDataType.recipient);

                  final formIsEmpty = sender.isEmpty || recipient.isEmpty;
                  return FilledButton(onPressed: formIsEmpty ? null : () {}, child: const Text('Next step'));
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
