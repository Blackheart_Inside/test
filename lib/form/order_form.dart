import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test/ui_kit/ui_kit.dart';

import 'model/address_info.dart';
import 'widget/post_user_card.dart';
import 'widget/text_field.dart';
import 'widget/text_field_wrapper.dart';

typedef OnChange = void Function(AddressInfo value);

enum OrederFormModel { fullname, email, phonenumber, country, city, address, passcode }

final class OrderFormWidget extends StatefulWidget {
  const OrderFormWidget(
      {super.key,
      this.title = '',
      this.onChange,
      this.model = const AddressInfo.empty(fullName: 'Name', addresses: ['Test address'])});

  final String title;
  final AddressInfo model;
  final OnChange? onChange;

  @override
  State<OrderFormWidget> createState() => _OrderFormWidgetState();
}

enum _OrderWidgetMode {
  addAdress,
  selectAdress;
}

class _OrderFormWidgetState extends State<OrderFormWidget> {
  late AddressInfo _model;
  late _OrderWidgetMode _mode;

  @override
  void initState() {
    _model = widget.model;
    _mode = _matchOrder(_model);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _model = widget.model;

    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant OrderFormWidget oldWidget) {
    if (oldWidget.model != widget.model) {
      _model = widget.model;
    }
    super.didUpdateWidget(oldWidget);
  }

  _OrderWidgetMode _matchOrder(AddressInfo model) =>
      model.isEmpty ? _OrderWidgetMode.addAdress : _OrderWidgetMode.selectAdress;

  void _changeMode(_OrderWidgetMode mode) {
    if (mode == _mode) return;
    setState(() {
      _mode = mode;
    });
  }

  Set<MaterialState>? resolveByMode(_OrderWidgetMode target) {
    if (_mode == target) return {MaterialState.selected};
    return null;
  }

  void _addNewAddressLine() => setState(() {
        final list = List<String>.of(_model.addresses)..add('');
        _model = _model.copyWith(addresses: list);
      });

  void _showWarringBanner() {
    final theme = Theme.of(context);
    final messager = ScaffoldMessenger.maybeOf(context);
    messager?.showMaterialBanner(MaterialBanner(
        backgroundColor: theme.colorScheme.errorContainer,
        content: Text(
          'Обязательные поля пустые!',
          style: TextStyle(color: theme.colorScheme.onErrorContainer),
        ),
        actions: [TextButton(onPressed: ScaffoldMessenger.maybeOf(context)?.clearMaterialBanners, child: Text('Ok'))]));
  }

  @override
  Widget build(BuildContext context) {
    final theme = context.theme;
    final textTheme = theme.textTheme;
    final paddings = context.paddings;

    final buttonStyle = ButtonStyle(
        minimumSize: const MaterialStatePropertyAll(Size.fromHeight(33)),
        foregroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.selected)) return null;
          return const Color(0xFF919EAB);
        }),
        backgroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.selected)) return null;
          return const Color(0xFFE7ECF0);
        }));

    return ColoredBox(
      color: theme.colorScheme.onBackground,
      child: Padding(
        padding: EdgeInsets.all(paddings.large),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: paddings.middle),
              child: DefaultTextStyle(
                  style: textTheme.bodyMedium!.copyWith(fontSize: paddings.middle, fontWeight: FontWeight.w700),
                  child: Text(widget.title)),
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: FilledButton(
                      style: buttonStyle,
                      statesController: MaterialStatesController(resolveByMode(_OrderWidgetMode.addAdress)),
                      onPressed: () {
                        _changeMode(_OrderWidgetMode.addAdress);
                      },
                      child: const Text('Add address')),
                ),
                const Padding(padding: EdgeInsets.only(right: 7)),
                Expanded(
                  child: FilledButton(
                      style: buttonStyle,
                      statesController: MaterialStatesController(resolveByMode(_OrderWidgetMode.selectAdress)),
                      onPressed: _model.isEmpty
                          ? _showWarringBanner
                          : () {
                              _changeMode(_OrderWidgetMode.selectAdress);
                            },
                      child: const Text('Select address')),
                )
              ],
            ),
            Padding(padding: EdgeInsets.only(top: paddings.middle)),
            switch (_mode) {
              _OrderWidgetMode.selectAdress => Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: paddings.small),
                      child: CustomTextField(
                        contentPadding: EdgeInsets.symmetric(vertical: 8.0, horizontal: paddings.small),
                        constraints: BoxConstraints.loose(const Size.fromHeight(37.0)),
                        icon: Icons.search,
                        iconSize: 16.0,
                        action: TextInputAction.search,
                        keyboardType: TextInputType.text,
                        maxLine: 1,
                        onChanged: (value) {},
                      ),
                    ),
                    PostUserCardWidget(model: _model, onChange: () => _changeMode(_OrderWidgetMode.addAdress))
                  ],
                ),
              _OrderWidgetMode.addAdress => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    //Fullname
                    TextFieldWrapper(
                      title: const Text('Full name*'),
                      textField: CustomTextField(
                        initialValue: _model.fullName,
                        icon: Icons.person,
                        action: TextInputAction.next,
                        keyboardType: TextInputType.name,
                        maxLine: 1,
                        onChanged: (value) {
                          widget.onChange?.call(_model.copyWith(fullName: value));
                        },
                      ),
                    ),
                    //Email
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: paddings.small),
                      child: TextFieldWrapper(
                        title: const Text('Email*'),
                        textField: CustomTextField(
                          initialValue: _model.email,
                          icon: Icons.mail,
                          action: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                          maxLine: 1,
                          onChanged: (value) {
                            widget.onChange?.call(_model.copyWith(email: value));
                          },
                        ),
                      ),
                    ),
                    //Phone number
                    TextFieldWrapper(
                      title: const Text('Phone number*'),
                      textField: CustomTextField(
                        initialValue: _model.phoneNumber,
                        icon: Icons.phone,
                        action: TextInputAction.next,
                        keyboardType: TextInputType.phone,
                        maxLine: 1,
                        inputFormatters: [LengthLimitingTextInputFormatter(11)],
                        onChanged: (value) {
                          widget.onChange?.call(_model.copyWith(phoneNumber: value));
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: paddings.middle, bottom: paddings.small),
                      child: const Divider(
                        color: Color(0xFFE7ECF0),
                        height: 1,
                      ),
                    ),
                    TextFieldWrapper(
                      title: const Text('Country*'),
                      textField: CustomTextField(
                        initialValue: _model.cuntry,
                        icon: Icons.place,
                        action: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        maxLine: 1,
                        onChanged: (value) {
                          widget.onChange?.call(_model.copyWith(cuntry: value));
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: paddings.small),
                      child: TextFieldWrapper(
                        title: const Text('City*'),
                        textField: CustomTextField(
                          initialValue: _model.city,
                          icon: Icons.location_city_outlined,
                          action: TextInputAction.next,
                          keyboardType: TextInputType.text,
                          maxLine: 1,
                          onChanged: (value) {
                            widget.onChange?.call(_model.copyWith(city: value));
                          },
                        ),
                      ),
                    ),
                    ListView.separated(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      cacheExtent: 44,
                      addAutomaticKeepAlives: false,
                      addRepaintBoundaries: false,
                      itemCount: _model.addresses.length,
                      itemBuilder: (context, index) {
                        final item = _model.addresses[index];
                        return TextFieldWrapper(
                            key: ValueKey(index),
                            title: Text('Address line ${index + 1}*'),
                            textField: CustomTextField(
                              initialValue: item,
                              action: TextInputAction.next,
                              keyboardType: TextInputType.streetAddress,
                              maxLine: 1,
                              icon: Icons.push_pin_outlined,
                              onChanged: (value) {
                                final addreses = List<String>.of(_model.addresses);
                                addreses[index] = value;

                                widget.onChange?.call(_model.copyWith(addresses: addreses));
                              },
                            ));
                      },
                      separatorBuilder: (_, __) => const Padding(padding: EdgeInsets.only(top: 12)),
                    ),
                    TextButton(onPressed: _addNewAddressLine, child: const Text('Add address line +')),

                    Padding(
                      padding: EdgeInsets.symmetric(vertical: paddings.small),
                      child: TextFieldWrapper(
                        title: const Text('Passcode*'),
                        textField: CustomTextField(
                          initialValue: _model.passcode,
                          icon: Icons.post_add,
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          maxLine: 1,
                          action: TextInputAction.done,
                          keyboardType: const TextInputType.numberWithOptions(signed: true),
                          onChanged: (value) {
                            widget.onChange?.call(_model.copyWith(passcode: value));
                          },
                        ),
                      ),
                    ),
                  ],
                )
            }
          ],
        ),
      ),
    );
  }
}
