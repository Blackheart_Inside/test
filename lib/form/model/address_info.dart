import 'package:flutter/foundation.dart' show listEquals;

final class AddressInfo {
  final String fullName;
  final String email;
  final String phoneNumber;
  final String cuntry;
  final String city;
  final List<String> addresses;
  final String passcode;

  const AddressInfo.empty(
      {this.fullName = '',
      this.email = '',
      this.phoneNumber = '',
      this.cuntry = '',
      this.city = '',
      this.addresses = const [''],
      this.passcode = ''});

  AddressInfo copyWith({
    String? fullName,
    String? email,
    String? phoneNumber,
    String? cuntry,
    String? city,
    List<String>? addresses,
    String? passcode,
  }) =>
      AddressInfo.empty(
          fullName: fullName ?? this.fullName,
          email: email ?? this.email,
          phoneNumber: phoneNumber ?? this.phoneNumber,
          cuntry: cuntry ?? this.cuntry,
          city: city ?? this.city,
          addresses: addresses ?? this.addresses,
          passcode: passcode ?? this.passcode);

  bool get isEmpty =>
      fullName.isEmpty ||
      email.isEmpty ||
      phoneNumber.isEmpty ||
      cuntry.isEmpty ||
      city.isEmpty ||
      addresses.isEmpty ||
      passcode.isEmpty;

  @override
  String toString() => "AddressInfo $fullName, $email, $phoneNumber, $cuntry, $city, $addresses, $passcode";

  @override
  int get hashCode => Object.hash(fullName, email, phoneNumber, cuntry, city, addresses, passcode);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddressInfo &&
          runtimeType == other.runtimeType &&
          fullName == other.fullName &&
          email == other.email &&
          phoneNumber == other.phoneNumber &&
          cuntry == other.cuntry &&
          city == other.city &&
          listEquals(addresses, other.addresses) &&
          passcode == other.passcode;
}
