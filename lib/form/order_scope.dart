import 'package:flutter/material.dart';

import 'model/address_info.dart';

abstract interface class IOrderScopeController {
  void changeSender(AddressInfo value);
  void changeRecipient(AddressInfo value);
  void changeStartDate(DateTime value);
}

final class OrderScope extends StatefulWidget {
  const OrderScope({super.key, required this.child});

  final Widget child;

  //Data scope

  //Order data
  static AddressInfo getByType(BuildContext context, OrderDataType type) =>
      _OrderInheritedModel.getByType(context, type) ?? _notFoundStateOfType();
  static AddressInfo? meybeGetByType(BuildContext context, OrderDataType type) =>
      _OrderInheritedModel.getByType(context, type);

  //Datetime
  static DateTime getStartDate(BuildContext context) =>
      _OrderInheritedModel.getStartDate(context) ?? _notFoundStateOfType();
  static DateTime? maybeGetStartDate(BuildContext context) => _OrderInheritedModel.getStartDate(context);

  //Scope controller
  static IOrderScopeController? maybeOf(BuildContext context) => context.findAncestorStateOfType<_OrderScopeState>();
  static IOrderScopeController of(BuildContext context) => maybeOf(context) ?? _notFoundStateOfType();

  static Never _notFoundStateOfType() => throw ArgumentError(
        'Out of scope, not found state of type OrderScope',
        'out_of_scope',
      );

  @override
  State<OrderScope> createState() => _OrderScopeState();
}

/// State for widget OrderScope
class _OrderScopeState extends State<OrderScope> implements IOrderScopeController {
  late ({AddressInfo sender, AddressInfo recipient}) _orderData;
  DateTime? _startDate;

  /* #region Lifecycle */
  @override
  void initState() {
    _orderData = (sender: const AddressInfo.empty(), recipient: const AddressInfo.empty());
    super.initState();
  }

  @override
  void changeStartDate(DateTime value) => setState(() {
        _startDate = value;
      });

  @override
  void changeSender(AddressInfo value) => setState(() {
        _orderData = (sender: value, recipient: _orderData.recipient);
      });

  @override
  void changeRecipient(AddressInfo value) => setState(() {
        _orderData = (sender: _orderData.sender, recipient: value);
      });

  @override
  Widget build(BuildContext context) => _OrderInheritedModel(
        values: _orderData,
        startDate: _startDate,
        child: widget.child,
      );
}

enum OrderDataType { sender, recipient, startDate }

final class _OrderInheritedModel extends InheritedModel<OrderDataType> {
  const _OrderInheritedModel({
    required this.values,
    required this.startDate,
    required super.child,
    super.key,
  });

  final ({AddressInfo sender, AddressInfo recipient}) values;
  final DateTime? startDate;

  static _OrderInheritedModel? maybeOf(BuildContext context, {bool listen = true}) => listen
      ? context.dependOnInheritedWidgetOfExactType<_OrderInheritedModel>()
      : context.getElementForInheritedWidgetOfExactType<_OrderInheritedModel>()?.widget as _OrderInheritedModel?;

  static AddressInfo? getByType(BuildContext context, OrderDataType type, {bool listen = true}) {
    if (listen) {
      final values = InheritedModel.inheritFrom<_OrderInheritedModel>(context, aspect: type)?.values;
      if (values == null) return null;
      return _getAspect(values, type);
    }

    final values = maybeOf(context, listen: false)?.values;
    return switch (values) { != null => _getAspect(values, type), _ => null };
  }

  static DateTime? getStartDate(BuildContext context, {bool listen = true}) {
    const type = OrderDataType.startDate;
    if (listen) {
      final values = InheritedModel.inheritFrom<_OrderInheritedModel>(context, aspect: type);
      if (values == null) return null;
      return values.startDate;
    }

    final values = maybeOf(context, listen: false);
    return values?.startDate;
  }

  @override
  bool updateShouldNotify(covariant _OrderInheritedModel oldWidget) =>
      oldWidget.values != values || oldWidget.startDate != startDate;

  @override
  bool updateShouldNotifyDependent(covariant _OrderInheritedModel oldWidget, Set<OrderDataType> aspects) {
    for (var aspect in aspects) {
      if (aspect == OrderDataType.startDate && startDate != oldWidget.startDate) return true;
      if ((_getAspect(values, aspect) != _getAspect(oldWidget.values, aspect))) return true;
    }
    return false;
  }

  static AddressInfo? _getAspect(({AddressInfo sender, AddressInfo recipient}) data, OrderDataType aspect) =>
      {OrderDataType.sender: data.sender, OrderDataType.recipient: data.recipient}[aspect];
}
