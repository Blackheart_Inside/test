import 'package:flutter/material.dart';

final class TextFieldWrapper extends StatelessWidget {
  const TextFieldWrapper({super.key, required this.title, required this.textField});

  final Widget title;
  final Widget textField;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: DefaultTextStyle(style: textTheme.bodyMedium!, child: title),
        ),
        ConstrainedBox(
          constraints: BoxConstraints.loose(const Size.fromHeight(44)),
          child: textField,
        )
      ],
    );
  }
}
