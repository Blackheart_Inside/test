import 'package:flutter/material.dart';
import 'package:test/form/model/address_info.dart';
import 'package:test/ui_kit/ui_kit.dart';

final class PostUserCardWidget extends StatelessWidget {
  const PostUserCardWidget({super.key, this.model = const AddressInfo.empty(), this.onChange});

  final AddressInfo model;
  final VoidCallback? onChange;

  @override
  Widget build(BuildContext context) {
    final paddings = context.paddings;
    return DecoratedBox(
      decoration: BoxDecoration(color: const Color(0xFF172027), borderRadius: BorderRadius.circular(12.0)),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: paddings.middle, vertical: paddings.large),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                    child: Text(
                  model.fullName,
                  style: const TextStyle(color: Colors.white),
                )),
                GestureDetector(
                  onTap: onChange,
                  child: const Icon(
                    Icons.edit_square,
                    size: 20,
                    color: Colors.white,
                  ),
                )
              ],
            ),
            ListView.separated(
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.only(top: 8),
                itemBuilder: (_, index) => Text(
                      model.addresses[index],
                      style: const TextStyle(fontSize: 14.0, fontWeight: FontWeight.w400, color: Color(0xFFFFFAF8)),
                    ),
                separatorBuilder: (_, __) => const Padding(padding: EdgeInsets.only(bottom: 8)),
                shrinkWrap: true,
                cacheExtent: 20,
                itemCount: model.addresses.length)
          ],
        ),
      ),
    );
  }
}
