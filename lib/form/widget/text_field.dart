import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {super.key,
      this.initialValue = '',
      this.icon,
      this.inputFormatters = const [],
      this.action = TextInputAction.none,
      this.keyboardType = TextInputType.none,
      this.maxLine = 1,
      this.contentPadding,
      this.constraints,
      this.iconSize = 20.0,
      this.readOnly = false,
      this.onTap,
      this.onChanged});

  final String initialValue;
  final IconData? icon;
  final List<TextInputFormatter> inputFormatters;
  final TextInputAction action;
  final TextInputType keyboardType;
  final int maxLine;
  final ValueChanged<String>? onChanged;
  final EdgeInsetsGeometry? contentPadding;
  final BoxConstraints? constraints;
  final double iconSize;
  final bool readOnly;
  final VoidCallback? onTap;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

/// State for widget CustomTextField
class _CustomTextFieldState extends State<CustomTextField> {
  late final TextEditingController _controller;

  /* #region Lifecycle */
  @override
  void initState() {
    _controller = TextEditingController(text: widget.initialValue);
    super.initState();
  }

  @override
  void didUpdateWidget(CustomTextField oldWidget) {
    if (oldWidget.initialValue != widget.initialValue) {
      final value = widget.initialValue;
      _controller.value = TextEditingValue(
        selection: _controller.value.selection,
        text: value,
      );
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  /* #endregion */

  @override
  Widget build(BuildContext context) => TextField(
        controller: _controller,
        decoration: InputDecoration(
            prefixIcon: Icon(
              widget.icon,
              size: widget.iconSize,
            ),
            contentPadding: widget.contentPadding,
            constraints: widget.constraints),
        readOnly: widget.readOnly,
        onTap: widget.onTap,
        inputFormatters: widget.inputFormatters,
        textInputAction: widget.action,
        keyboardType: widget.keyboardType,
        maxLines: widget.maxLine,
        onChanged: widget.onChanged,
      );
}
